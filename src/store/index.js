import Vue from 'vue'
import Vuex from "vuex"

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        basePrice: 6,
        totalPrice: 6,
        ingredients: []
    },
    mutations: {
        setIngredients (state, ingredients) {
            state.ingredients = ingredients;
        },
        increment (state, id) {
            state.ingredients.forEach(function(ingredient) {
                if (id === ingredient.id) {
                    ingredient.count++;
                }
            });
            this.commit('refreshTotalPrice');
        },
        decrement (state, id) {
            state.ingredients.forEach(function(ingredient) {
                if (id === ingredient.id && ingredient.count > 0) {
                    ingredient.count--;
                }
            });
            this.commit('refreshTotalPrice');
        },
        refreshTotalPrice (state) {
            state.totalPrice = state.basePrice;
            state.ingredients.forEach(function(ingredient) {
                state.totalPrice += ingredient.price * ingredient.count;
            });
        }
    }
})